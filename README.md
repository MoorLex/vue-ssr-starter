# project-name

> Vue.js SSR template

## Build Setup 

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:7070
npm run dev

# build for production with minification
npm run build
```