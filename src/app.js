import Vue from 'vue'
import Meta from 'vue-meta'
import App from './App.vue'
import { createStore } from './store'
import { createRouter } from './router'
import { sync } from 'vuex-router-sync'
import NoSSR from 'vue-no-ssr'

Vue.use(Meta);
Vue.component('no-ssr', NoSSR);

Vue.config.productionTip = false;

export function createApp () {

  const store = createStore();
  const router = createRouter(store);
  sync(store, router);

  const app = new Vue({
    metaInfo: {
      title: 'Vue SSR Starter',
      meta: [
        { vmid: 'description', name: 'description', content: 'vue ssr template' }
      ],
      htmlAttrs: {
        lang: 'en'
      }
    },
    router,
    store,
    render: h => h(App)
  });

  return { app, router, store }
}
