import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router);

export function createRouter (store) {

  return new Router({
    routes: routes(store),
    mode: 'history',
    fallback: false,
    scrollBehavior (to, from, savedPosition)
    {
      if (savedPosition) return savedPosition;
      else if (to.hash) return { selector: to.hash };
      else return { x: 0, y: 0 }
    }
  })
}
