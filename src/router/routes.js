const _import = file => () => import(`../templates/pages/${file}`);

export default function (store) {
  return [
    {
      path: '/',
      name: 'Home',
      component: _import('Home')
    }
  ]
}
