require('dotenv').config();
const fs = require('fs');
const path = require('path');
const LRU = require('lru-cache');
const express = require('express');
const favicon = require('serve-favicon');
const compression = require('compression');
const microcache = require('route-cache');
const portfinder = require('portfinder');
const clc = require("cli-color");
const resolve = file => path.resolve(__dirname, file);
const { createBundleRenderer } = require('vue-server-renderer');

const isProd = process.env.NODE_ENV === 'production';
const port = process.env.PORT || 3000;
const useMicroCache = process.env.MICRO_CACHE !== 'false';
const serverInfo =
  `express/${require('express/package.json').version} ` +
  `vue-server-renderer/${require('vue-server-renderer/package.json').version}`;

const success = clc.bold.green;
const error = clc.bold.red;
const warn = clc.yellow;
const notice = clc.blue;

const app = express();
app.disable('etag');

function createRenderer (bundle, options) {

  return createBundleRenderer(bundle, Object.assign(options, {
    // for component caching
    cache: LRU({
      max: 1000,
      maxAge: 1000 * 60 * 15
    }),
    // this is only needed when vue-server-renderer is npm-linked
    basedir: resolve('./dist'),
    // recommended for performance
    runInNewContext: false,
    inject: true
  }))
}

let renderer;
let readyPromise;
const templatePath = resolve('./templates/index.html');

if (isProd) {
  const template = fs.readFileSync(templatePath, 'utf-8');
  const bundle = require('./dist/vue-ssr-server-bundle.json');
  const clientManifest = require('./dist/vue-ssr-client-manifest.json');
  renderer = createRenderer(bundle, {
    template,
    clientManifest
  })
} else {
  readyPromise = require('./build/setup-dev-server')(
    app,
    templatePath,
    (bundle, options) => { renderer = createRenderer(bundle, options) }
  )
}

const serve = (path, cache) => express.static(resolve(path), {
  maxAge: cache && isProd ? 1000 * 60 * 60 * 24 * 30 : 0
});

app.use(compression({ threshold: 0 }));
app.use(favicon('./public/favicon.ico'));
app.use('/dist', serve('./dist', true));
app.use('/public', serve('./public', true));
app.use('/manifest.json', serve('./manifest.json', true));
app.use('/service-worker.js', serve('./dist/service-worker.js'));
app.use('/robots.txt', serve('./public/robots.txt'));

app.use(microcache.cacheSeconds(1, req => useMicroCache && req.originalUrl));

function render (req, res) {

  const s = Date.now();
  const context = { url: req.url };

  res.setHeader('Content-Type', 'text/html');
  res.setHeader('Server', serverInfo);

  const handleError = err => {

    let data = {};

    if (err.url) res.redirect(err.url);
    else if (err.code === 404){
      res.status(404);
      data['code'] = 404;
      data['message'] = 'Page Not Found';
    }
    else {
      res.status(500);
      data['code'] = 500;
      data['message'] = 'Internal Server Error';
      console.error(`error during render : ${req.url}`);
      console.error(err.stack)
    }

    const errorPath = resolve('./templates/error_'+data['code']+'.html');
    fs.readFile(errorPath, (err, html) => {
      html = html.toString();

      Object.keys(data).map(key => {
        let re = new RegExp('{{'+key+'}}',"g");
        html = html.replace(re, data[key]);
      });

      res.send(`<!DOCTYPE html> <html lang="en"> ${html} </html>`);
    });
  };

  renderer.renderToString(context, (err, html) => {

    if (err)
      return handleError(err);

    const { htmlAttrs } = context.meta.inject();

    res.send(`<!DOCTYPE html> <html data-vue-meta-server-rendered ${htmlAttrs.text()}>  ${html} </html>`);

    if (!isProd)
      console.log(`whole request: ${Date.now() - s}ms`)
  })
}

app.get('*', isProd ? render : (req, res) => {
  readyPromise.then( () => render(req, res) )
});

portfinder.basePort = port;
portfinder.getPort((err, port) => {
  if (!err) {
    process.env.PORT = port;
    app.listen(port, () => {
      console.log(success(`[+] Server started at: `), `http://localhost:${port}`)
    });
  } else {
      console.log(error(`[x] Server error:`));
      console.log(err);
  }
});
